package com.epstim.rss.reader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Alan Żur on 2014-05-19.
 */
public class ArticlesAdapter extends ArrayAdapter<Article> {
    Context context;
    List<Article> articles;

    public ArticlesAdapter(Context context, int resource, List<Article> objects) {
        super(context, resource, objects);
        this.context = context;
        this.articles = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = LayoutInflater.from(context).inflate(R.layout.row_articles, parent,false);
        TextView title = (TextView) row.findViewById(R.id.title);
        TextView description = (TextView) row.findViewById(R.id.description);
        title.setText(articles.get(position).getTitle());
        description.setText(articles.get(position).getLink());
        return row;
    }
}
