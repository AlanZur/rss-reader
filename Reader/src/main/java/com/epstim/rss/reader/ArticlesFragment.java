package com.epstim.rss.reader;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Created by Alan Żur on 2014-05-19.
 */
public class ArticlesFragment extends Fragment implements AdapterView.OnItemClickListener {

    ListView mListView;

    public ArticlesFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_articles, container, false);
        mListView = (ListView) rootView.findViewById(R.id.articlesList);
        mListView.setOnItemClickListener(this);
        mListView.setDividerHeight(2);
        new RSSTask().execute("http://www.spidersweb.pl/category/nowe-kategorie/hardware-nowe-kategorie/feed");
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListView.setItemChecked(position, true);
        DetailsFragment detailsFragment = DetailsFragment.newInstance(((Article)parent.getItemAtPosition(position)).getLink());
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        ft.replace(R.id.container, detailsFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    private class RSSTask extends AsyncTask<String, Void, List<Article>>{
        DialogFragment dialog;

        @Override
        protected void onPreExecute() {
            dialog = AlertDialogFragment.newInstance();
            dialog.show(getFragmentManager(), "dialog");
        }

        @Override
        protected List<Article> doInBackground(String... params) {
            try {
                RSSHandler rss = new RSSHandler(params[0]);
                return rss.getArticles();
            } catch (MalformedURLException e) {
                Log.e("URL", "error: " + e.toString());
            } catch (XmlPullParserException e) {
                Log.e("XML", "error: " + e.toString());
            } catch (IOException e) {
                Log.e("IO", "error: " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Article> articles) {
            //mListView.setAdapter(new ArticlesAdapter(ArticlesFragment.this.getActivity().getApplicationContext(), R.layout.row_articles, articles));
            mListView.setAdapter(new ArrayAdapter<Article>(getActivity(), android.R.layout.simple_list_item_1, articles));
            mListView.setOnItemClickListener(ArticlesFragment.this);
            dialog.dismiss();
        }
    }
}
